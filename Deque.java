import java.util.ArrayDeque;

public class Deque {

    public static void main(String[] args) {
        java.util.Deque<String> dq = new ArrayDeque<>();
        dq.add("Juan");
        dq.add("Luis");
        dq.add("Pedro");

        String x = dq.peekLast();
        System.out.println(x);

         
        x = dq.pollLast();
        System.out.println(x);

        //Estos métodos se utilizan para obtener objetos de la cola(deque)
        x = dq.peekFirst();
        System.out.println(x);

        x = dq.getFirst();
        System.out.println(x);


        //Estos métodos se utilizan para añadir objetos a la cola(deque)
        dq.offerFirst("Jose");
        dq.offerLast("perro");
        dq.addFirst("Leider");
        dq.addLast("Luisa");
        System.out.println(dq);

        //Estos son algunos de los métodos implementados en la cola(deque)

    }   

}
